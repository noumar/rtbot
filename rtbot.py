#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
RT(IR)bot for AbuseHelper
"""

__author__ = "Mikael Karlsson <i8myshoes@gmail.com>"

import logging
import datetime
import rt  # python-rt library
import secret  # Secrets
import idiokit
from abusehelper.core import bot, taskfarm, events

# TODO: All '%' string formatting should be changed to .format(), for future Python3 compatibility!


class RTIRBot(bot.ServiceBot):
    def __init__(self, *args, **keys):
        super(RTIRBot, self).__init__(*args, **keys)

        # self.tracker = rt.Rt(secret.host_prod, secret.username, secret.password)
        self.tracker = rt.Rt(secret.host_test, secret.username, secret.password)
        self.tracker.session.verify = "/home/{0}/ca.crt".format(secret.username)
        self.tracker.login()
        # self.tracker.logout()
        self.max_age_days = 14
        self.rooms = taskfarm.TaskFarm(self.handle_room)

    @idiokit.stream
    def handle_room(self, name):
        msg = "room {0!r}".format(name)
        attrs = events.Event(type="room", service=self.bot_name, room=name)

        with self.log.stateful(repr(self.xmpp.jid), "room", repr(name)) as log:
            log.open("Joining " + msg, attrs, status="joining")
            room = yield self.xmpp.muc.join(name, self.bot_name)

            log.open("Joined " + msg, attrs, status="joined")
            try:
                room_jid = room.jid.bare()
                yield room | events.stanzas_to_events() | self.collect(room_jid)
            finally:
                log.close("Left " + msg, attrs, status="left")

    @idiokit.stream
    def session(self, state, src_room):
        yield self.rooms.inc(src_room)

    def collect(self, room_name):
        collect = self._collect(room_name)
        idiokit.pipe(self._alert(), collect)
        return collect

    @idiokit.stream
    def _alert(self, flush_interval=2.0):
        while True:
            yield idiokit.sleep(flush_interval)
            yield idiokit.send()

    @idiokit.stream
    def _collect(self, room_name):
        try:
            while True:
                event = yield idiokit.next()
                if event is None:
                    continue
                self._process(
                    {
                        "ip": event.values("ip"),
                        "timestamp": event.values("time"),
                        "type": event.values("type")
                    })
        finally:
            pass

    def _process(self, record):
        def clean_ticket_id(text):
            """
            Extract numeric ticket id from other text
            :type text: str
            """
            return int(text.split("/")[-1])

        def time_to_datetime(timestamp):
            """
            Return a datetime object from timestamp string
            :type timestamp: str
            """
            return datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")

        def split_ticket_ip(ip):
            """
            Split CF_IP result into separated IPs as iterator
            :type ip: str
            """
            return iter(ip.split(","))

        def is_investigation(ticket_id):
            """
            :rtype : bool
            :type ticket_id: int
            """
            return self.tracker.get_ticket(ticket_id)["Queue"] == "Investigations"

        # TODO: This is an example templater using string formatting, other option would be string templates
        def templater(ticket_type, field_type, report_type):
            """
            :rtype : str
            """
            # TODO: Read from some file
            with open("templates/template_{0}_{1}_{2}.txt".format(ticket_type, field_type, report_type), "r") as tf:
                text = tf.read()

            # TODO: Do some processing
            return text

        # An incoming "record" should be transformed to this format, i.e a dictionary
        # record = {"ip":xxx, "timestamp":yyy, "type":zzz}
        logging.basicConfig(level=logging.DEBUG)

        time_now = datetime.datetime.utcnow()
        time_limit = (datetime.datetime.utcnow() - datetime.timedelta(days=self.max_age_days))
        time_record = time_to_datetime(record["time"])
        logging.debug("now %s" % time_now)
        logging.debug("%d days ago %s" % (self.max_age_days, time_limit))
        logging.debug("record time %s" % time_record)

        # Check age of record
        if time_record < time_limit:
            logging.info("Record is obsolete! '%s' old and '%s' overdue"
                         % ((time_now - time_record), (time_limit - time_record)))
            logging.info("End processing!")
            return

        # Try to find "Incident Report"
        # TODO: Created/Started/Resolved/Starts only supports day level!
        reports_found = self.tracker.search(
            Queue="Incident Reports",
            Query="(CF.{IP}='%s'"
                  " AND (Status='new' OR Status='open' OR Status='resolved')"
                  " AND Starts='%s')"
                  % (record["ip"], record["time"]))
        logging.debug(reports_found)

        # If we have seen this exact "record" before then this is "The End"
        if reports_found:
            logging.debug("RT's suggested Incident Report(s)! id(s)=%s"
                          % [clean_ticket_id(report["id"]) for report in reports_found])

            # This is an attempt to catch RT's silly matching of Timestamp based values as only Dates
            real_reports_found = [clean_ticket_id(report["id"])
                                  for report in reports_found if report["Starts"] == record["time"]]
            if real_reports_found:
                logging.debug("Really matching Incident Report(s)! id(s)=%s" % real_reports_found)
                logging.info("End processing!")
                return
            else:
                logging.debug("'Starts' mismatched! Continuing processing!")

        # Create new "Incident Report"
        # i.e new ticket in above queue
        # TODO: Resolved Incident Reports seem to contain a "Resolved" value, should this be used somewhere?
        report_id = self.tracker.create_ticket(
            Queue="Incident Reports",
            Owner=secret.owner,
            Subject=templater("report", "subject", record["type"]).format(
                type=record["type"].capitalize(),
                ip=record["ip"],
                time=record["time"]),
            Text=templater("report", "content", record["type"]).format(
                ip=record["ip"],
                time=record["time"],
                type=record["type"].capitalize()),
            Starts=record["time"],
            CF_IP=record["ip"])

        if report_id:
            logging.info("Created Incident Report! id=%s" % report_id)
        else:
            logging.error("Could not create Incident Report!")
            logging.error("RT might still have created the ticket! Please verify this manually!")
            return

        # Try to find open "Incident" to which "record" belongs
        # If not found create new "Incident" and "Investigation"
        # i.e new ticket in above queues
        incidents_found = self.tracker.search(
            Queue="Incidents",
            Query="(CF.{{IP}}='{0}'"
                  " AND Status='open')".format(record["ip"]))
        logging.debug(incidents_found)
        if incidents_found:
            logging.debug("RT's suggested Incident(s)! id(s)={0}".format(
                [clean_ticket_id(i["id"]) for i in incidents_found]))

            # Discard range matches that RT has returned
            incidents_found = [i for i in incidents_found
                               for s in split_ticket_ip(i["CF.{IP}"]) if s == record["ip"]]
            logging.debug("Really matching Incident(s)! id(s)={0}".format(
                [clean_ticket_id(i["id"]) for i in incidents_found]))

            # Check time validity of Incidents
            active_incidents = [i for i in incidents_found
                                if time_record > time_to_datetime(i["LastUpdated"])]
            if active_incidents:
                logging.debug("Matching active Incident(s)! id(s)={0}".format(
                    [clean_ticket_id(i["id"]) for i in active_incidents]))
            else:
                logging.debug("No active Incident found!")
                logging.info("End processing!")
                return

            # Choose the Incident with latest 'Created' field
            incident = clean_ticket_id(max(active_incidents, key=lambda x: time_to_datetime(x["Created"]))["id"])
            logging.debug("The golden Incident! id={0}".format(incident))

            # Find the "Investigation" related to "Incident"
            logging.debug("Ticket links: {0}".format(self.tracker.get_links(incident)))
            investigations = [clean_ticket_id(t)
                              for t in self.tracker.get_links(incident)["Members"]
                              if is_investigation(clean_ticket_id(t))]
            logging.debug("Investigations found: {0}".format(investigations))
            if len(investigations) > 1:
                logging.debug("More than 1 investigation found!")
                logging.info("End processing!")
                return
            else:
                investigation = investigations[0]
        else:
            # TODO: Create a new Incident
            incident = self.tracker.create_ticket(
                Queue="Incidents",
                Owner=secret.owner,
                Subject=templater("incident", "subject", record["type"]).format(
                    ip=record["ip"],
                    time=record["time"],
                    type=record["type"]),
                Text=templater("incident", "content", record["type"]).format(
                    ip=record["ip"],
                    time=record["time"],
                    type=record["type"]))
            # TODO: Create a new Investigation
            investigation = self.tracker.create_ticket(
                Queue="Investigation",
                Owner=secret.owner,
                Subject=templater("investigation", "subject", record["type"]).format(
                    ip=record["ip"],
                    time=record["time"],
                    type=record["type"]),
                Text=templater("investigation", "content", record["type"]).format(
                    ip=record["ip"],
                    time=record["time"],
                    type=record["type"]))

        # Link "Incident Report" to "Incident"
        if self.tracker.edit_ticket_links(report_id, MemberOf=incident):
            logging.debug("Incident Report linked to Incident!")
        else:
            logging.warning("Incident Report could not be linked to Incident!")
            logging.error("Linking {0} -> {1} will most likely need humanoid action!".format(report_id, incident))

        # TODO: Update Investigation
        # TODO: Should we use 'reply' or 'comment' for this?
        self.tracker.comment(
            investigation,
            Text=templater("investigation", "comment", record["type"]).format(
                ip=record["ip"],
                time=record["time"],
                type=record["type"]))


if __name__ == "__main__":
    RTIRBot.from_command_line().execute()
