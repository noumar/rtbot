#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import pprint
import rt
import secret

# tracker = rt.Rt(secret.host_prod, secret.username, secret.password)
tracker = rt.Rt(secret.host_test, secret.username, secret.password)
tracker.session.verify = "/home/%s/ca.crt" % secret.username
tracker.login()
# pprint.pprint(tracker.search(Queue="Incident Reports",
#                              Query="CF.{IP}='94.237.89.112' AND (Status='open' OR Status='resolved')"))
# pprint.pprint(tracker.search(Queue="Incident Reports",
#                              Query="CF.{IP}='94.237.89.112' AND Status='open'"))

# pprint.pprint(tracker.search(Queue="Alerts"))
pprint.pprint(tracker.get_ticket(90185))

#tracker.create_ticket(Queue="Alerts", Owner="owner", Subject="Mikael testailee", Text="Pälä pälä pälä", CF_Time="2013-02-18 09:36:47")
